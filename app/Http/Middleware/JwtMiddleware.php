<?php

namespace App\Http\Middleware;
use Tymon\JWTAuth\JWTAuth;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use Closure;

class JwtMiddleware extends BaseMiddleware
{
     /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        try {
            //$this->auth->parseToken();
            //$this->auth->parseToken()->authenticate();
            $this->authenticate($request);
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                return response()->json(['message' => $e->getMessage(), 'error' => true], 403);
            } else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                return response()->json(['message' => $e->getMessage(), 'error' => true], 403);
            } else {
                return response()->json(['message' => $e->getMessage(), 'error' => true], 403);
            }
        }
        // $user = $this->auth->user();

        // if($user->banned == true)
        //     return response()->json(['message' => 'user banned' , 'error' => true] , 403);

        return $next($request);
    }
}
