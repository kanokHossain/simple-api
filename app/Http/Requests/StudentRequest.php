<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required",
            "roll_no" => "required|numeric"
        ];
    }
     // this function is to send force sending json response on validation failure
     protected function failedValidation(Validator $validator)
     {
         $errors = (new ValidationException($validator))->errors();

         throw new HttpResponseException(
             response()->json(['success'=>false, 'errors' => $errors], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
         );
     }
}
