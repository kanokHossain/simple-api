<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function responseWithSuccess($message = "", $code = 200 , $data = [] ){

        return response()->json([
                'success' => true,
                'message'=> $message,
                'data' =>  $data,
        ],$code);
    }

    protected function responseWithError($message = "", $code = 400 , $data = [] ){

        return response()->json([
                'success' => true,
                'message'=> $message,
                'data' =>  $data,
        ],$code);
    }
}
