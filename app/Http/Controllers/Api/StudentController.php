<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StudentRequest;
use App\Http\Resources\Student as ResourcesStudent;
use App\Http\Resources\StudentCollection;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StudentController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt');
    }

    public function postStudentData(StudentRequest $request){

        // try{
        $student = new Student();
        $student->name = $request->name;
        $student->roll_no = $request->roll_no;
        $student->save();
        // return $student;

        return new ResourcesStudent($student);

        // $data = [
        //     "name_is" => $student->name
        // ];
        // catch(/Exception $e){
        //     return $this->responseWithError($e->message(), 400, $data);
        // }

        // return $this->responseWithSuccess("success", 200, $data);

        // Student::insert(["name" => $request->name, "roll_no" => $request->roll_no]);

    }
    public function getStudentData(){
        // return Auth::gurad('api')->user();
        $students = Student::all();
        return new StudentCollection($students);
        // return $students;
        // ddd($students)->toArray();
        // foreach($students as $key=>$row){
        //     $data[$key] = [
        //         "name_is" => $row->name
        //     ];
        // }
        // return $data;
        // return $this->responseWithSuccess("success", 200, $data);
    }
    public function getStudentById($id){
        $student = Student::where("id", $id)->first();
        // $student = Student::find($id);
        return new ResourcesStudent($student);
    }


}
